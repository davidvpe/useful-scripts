#! /bin/bash

dir=$1

check_folder() {
	echo "Checking folder $1"
	for file in "$1"/*
	do
		if [ -d $file ]
		then
			check_folder $file
		else
			if [[ "$file" == *.png || "$file" == *.jpg ]]
			then
				short=$(basename -- "$file")
				short_without="${short%.*}"
				full_without="${file%.*}"
				cwebp -q 80 "$file" -o "$full_without.webp"
			fi
		fi
	done
}

echo "                                                                    "
echo "                                                                    "
echo "By: David Velarde <davidvpe@gmail.com>"
echo "                                                                    "
echo "                                                                    "

check_folder $dir