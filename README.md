# Useful Scripts

This repository will hold all the scripts I've needed along my web development, devops, and life journey.

#### convert_images_to_webp.sh
This script will allow me to check all images recursively in a given folder, create webp files from them, and place them right beside the source file. This will only work for .png and .jpeg for now.

**Important**

In order for this script to work, **cwebp** must be installed and added to the path
```https://chromium.googlesource.com/webm/libwebp```

```bash
$ chmod +x convert_images_to_webp.sh
$ ./convert_images_to_webp.sh ~/awesome_images_folder
```

If you find this usefull leave a ⭐

If you want to make some changes or increase the functionality feel free to leave a PR ↩️
